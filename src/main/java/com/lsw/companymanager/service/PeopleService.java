package com.lsw.companymanager.service;

import com.lsw.companymanager.entity.People;
import com.lsw.companymanager.model.PeopleItem;
import com.lsw.companymanager.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public void setPeople(String name, String phone, LocalDate birthday) {
        People addData = new People();
        addData.setName(name);
        addData.setPhone(phone);
        addData.setBirthday(birthday);

        peopleRepository.save(addData);//이름(peopleRep~) 부르기 잊지말기
    }

    public List<PeopleItem> getpeoples(){
        List<PeopleItem> result = new LinkedList<>();

        List<People> originData = peopleRepository.findAll();

        for(People item : originData) {
            PeopleItem additem = new PeopleItem();
            additem.setName(item.getName());
            additem.setPhone(item.getPhone());

            result.add(additem);
        }

        return result;
    }

}
