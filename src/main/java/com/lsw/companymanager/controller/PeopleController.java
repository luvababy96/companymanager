package com.lsw.companymanager.controller;

import com.lsw.companymanager.model.PeopleItem;
import com.lsw.companymanager.model.PeopleRequest;
import com.lsw.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/data")
    public String setPeople(@RequestBody @Valid PeopleRequest request) {
        peopleService.setPeople(request.getName(), request.getPhone(), request.getBirthday());
        return "OK";
    }

    @GetMapping("/peoples")
    public List<PeopleItem> getpeoples() {
        List<PeopleItem> result = peopleService.getpeoples();
        return result;
    }
}
