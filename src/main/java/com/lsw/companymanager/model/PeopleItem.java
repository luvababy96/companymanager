package com.lsw.companymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PeopleItem {
    @NotNull
    @Length(min = 2, max = 20)
    private String name;
    @NotNull
    @Length(min = 11, max = 20)
    private String phone;

}
