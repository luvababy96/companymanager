package com.lsw.companymanager.repository;

import com.lsw.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
